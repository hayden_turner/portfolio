"use client";
import React, { useTransition, useState } from "react";
import Image from "next/image";
import TabButton from "./TabButton";

const TAB_DATA = [
  {
    title: "Skills",
    id: "skills",
    content: (
      <ul className="list-disc pl-2">
        <li>AWS</li>
        <li>Terraform</li>
        <li>Puppet</li>
        <li>Linux</li>
        <li>Bash</li>
        <li>Python</li>
        <li>Security</li>
      </ul>
    ),
  },
  {
    title: "Education",
    id: "education",
    content: (
      <ul className="list-disc pl-2">
        <li>MS in Cybersecurity | Western Governors University</li>
        <li>BS in Information Technology | Grand Canyon University</li>
      </ul>
    ),
  },
  {
    title: "Certifications",
    id: "certifications",
    content: (
      <ul className="list-disc pl-2">
        <li>ISC2 CC</li>
        <li>CompTIA Security+</li>
        <li>CompTIA CySa+</li>
        <li>CompTIA Pentest+</li>
      </ul>
    ),
  },
];

const AboutSection = () => {
  const [tab, setTab] = useState("skills");
  const [isPending, startTransition] = useTransition();

  const handleTabChange = (id) => {
    startTransition(() => {
      setTab(id);
    });
  };

  return (
    <section className="text-white" id="about">
      <div className="md:grid md:grid-cols-2 gap-8 items-center py-8 px-4 xl:gap-16 sm:py-16 xl:px-16">
        <Image src="/images/carbon.png" width={500} height={500} />
        <div className="mt-4 md:mt-0 text-left flex flex-col h-full">
          <h1 className="text-4xl font-bold text-white mb-4">About Me</h1>
          <p className="text-base lg:text-lg">
          In 2017 I joined the U.S. Navy as an Information Systems Technician. This started my
          journey in the tech field. Since then I have built and maintained a diverse list of systems
          and services.
          <br></br>
          <br></br>
          I currently work as a Cloud Engineer, leveraging infrastructure as code and AWS to maintain a large environment.
          I enjoy using Python and other tools to automate some of my tasks.
          <br></br>
          <br></br>
          I also have an interest in security and pentesting. A lot of my projects relate to security in some way.
          </p>
          <div className="flex flex-row justify-start mt-8 text-2xl font-bold">
            <TabButton
              selectTab={() => handleTabChange("skills")}
              active={tab === "skills"}
            >
              {" "}
              Skills{" "}
            </TabButton>
            <TabButton
              selectTab={() => handleTabChange("education")}
              active={tab === "education"}
            >
              {" "}
              Education{" "}
            </TabButton>
            <TabButton
              selectTab={() => handleTabChange("certifications")}
              active={tab === "certifications"}
            >
              {" "}
              Certifications{" "}
            </TabButton>
          </div>
          <div className="mt-8">
            {TAB_DATA.find((t) => t.id === tab).content}
          </div>
        </div>
      </div>
    </section>
  );
};

export default AboutSection;
