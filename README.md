# Porfolio
This is a [Next.js](https://nextjs.org/) project based on the portfolio project and guide created by [JudyGab](https://github.com/judygab/nextjs-portfolio)

## Deployment

Currently deployed via the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.
